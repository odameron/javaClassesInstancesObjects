

/**
 * Simple student with an unique identifier and a grade.
 * 
 * @author Olivier Dameron
 *
 */
public class Student {
	
	private String ident;
	private double grade;
	
	/**
	 * creates a student with his/her identifier and grade.
	 * 
	 * @param identifier
	 * @param grade
	 */
	public Student(String identifier, double aGrade) {
		this.ident = identifier;
		this.grade = aGrade;
	}
	
	public Student(String identifier) {
		Student(identifier, 0.);
	}
	
	public String getIdentifier() {
		return this.ident;
	}
	
	public double getGrade() {
		return this.grade;
	}
	
	public void setGrade(double newGrade) {
		this.grade = newGrade;
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Student donald = new Student("Donald", 10.5);
	}

}

