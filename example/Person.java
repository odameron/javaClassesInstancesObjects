

/**
 * Simple person
 * 
 * @author Olivier Dameron
 *
 */
public class Person {
	
	private int ident;
	private String name;
	
	/**
	 * creates a person with an identifier and a name
	 * 
	 * @param identifier
	 * @param name
	 */
	public Person(int identifier, String aName) {
		this.ident = identifier;
		this.name = aName;
	}
	
	public int getIdentifier() {
		return this.ident;
	}
	
	public String getName() {
		return this.name;
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Person firstPerson = new Person(42, "Hagrid"); 
		Person secondPerson = new Person(43, "Hermione");
		Person thirdPerson = secondPerson;
		Person fourthPerson = new Person(43, "Hermione");	

		System.out.println(firstPerson);
		System.out.println(secondPerson);
		System.out.println(thirdPerson);

		System.out.println("");
		System.out.println(secondPerson == firstPerson);    // false
		System.out.println(secondPerson == thirdPerson);    // true (same object)
		System.out.println(secondPerson == fourthPerson);    // false (different objects)
	}

}

