# Classes, instances and objects in java

A short presentation on the differences between classes, objects and instances in Java.

## Todo

- [ ] `src` directory with code examples
- [ ] class `Person` test `latestSSID` as static with automatic increment in constructor `Person()` called by `super()` in all the other constructors
- [ ] complete attribute description in the 'Class' slide
